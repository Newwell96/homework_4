# _Домашнее задание №4_

В макете осталось ещё очень много несвёрастанных страничек. Необходимо:

* Сверстать страницу авторизации.
* Отрефакторить готовую с текущего урока страничку игрового поля.
* Сверстать страничку с рейтингом взяв шапку из странички игрового поля.
* Сделать себе публичный репозиторий в гитлаб для сверстанных страничек и залить туда.
* Ссылку на репозиторий прикрепить к ДЗ.

По итогу у вас получится три отдельных HTML странички. Общая папка с svg для них. И общие CSS-файлы стилей.